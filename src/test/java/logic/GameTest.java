package logic;

import app.character.Figure;
import app.logic.Game;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static app.common.AppConstants.HERO_COORDINATE;
import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    void should_generate_world_without_objects_intersections() {
        //given
        var game = new Game();

        //when
        game.generateWorld();

        //then
        var mobCoordinates = game.getMobs().stream().map(Figure::getCoordinate).collect(Collectors.toSet());
        var hasMobCoordinatesIntersection = game.getFoodCoordinates()
                                                .stream()
                                                .anyMatch(coordinate -> coordinate.equals(HERO_COORDINATE));
        assertTrue(!mobCoordinates.isEmpty());
        assertFalse(hasMobCoordinatesIntersection);

        var hasFoodCoordinateIntersection = game.getFoodCoordinates()
                                                .stream()
                                                .anyMatch(coordinate -> coordinate.equals(HERO_COORDINATE) || mobCoordinates.contains(coordinate));
        assertFalse(game.getFoodCoordinates().isEmpty());
        assertFalse(hasFoodCoordinateIntersection);
    }
}
