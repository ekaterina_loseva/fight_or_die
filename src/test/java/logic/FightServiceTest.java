package logic;

import app.character.hero.Magician;
import app.character.hero.Player;
import app.character.mob.ArcherMob;
import app.character.mob.MagicianMob;
import app.character.mob.Mob;
import app.logic.Attack;
import app.logic.FightService;
import app.logic.Game;
import app.ui.GameMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FightServiceTest {

    @InjectMocks
    private FightService fightService;

    private Game game;
    private Player player;
    private Mob mob;

    @BeforeEach
    void setUp() {
        game = new Game();
        player = new Magician();
        mob = new ArcherMob(new GameMap.Coordinate(0, 0));
        game.setPlayer(player);
        game.addMob(mob);
    }

    @Test
    void should_finish_game_when_player_is_dead() {
        //given
        player.setHealth(1);

        var attackRegistry = new ArrayList<Attack>();
        attackRegistry.add(new Attack(mob, player.getDamage(), mob.getHealth() - player.getDamage()));
        attackRegistry.add(new Attack(player, mob.getDamage(), player.getHealth() - mob.getDamage()));

        //when
        fightService.fight(game, player, mob);

        //then
        assertTrue(player.isDead());
        assertTrue(game.isFinished());
        assertFalse(mob.isDead());
        assertEquals(1, game.getMobs().size());
        assertEquals(attackRegistry.size(), game.getAttackRegistry().size());
        checkAttackRegistryEntry(attackRegistry, 0);
        checkAttackRegistryEntry(attackRegistry, 1);
    }

    @Test
    void player_should_win_when_all_mobs_are_dead() {
        //given
        mob.setHealth(1);

        var attackRegistry = new ArrayList<Attack>();
        attackRegistry.add(new Attack(player, mob.getDamage(), player.getHealth() - mob.getDamage()));
        attackRegistry.add(new Attack(mob, player.getDamage(), mob.getHealth() - player.getDamage()));

        //when
        fightService.fight(game, mob, player);

        //then
        assertTrue(mob.isDead());
        assertTrue(game.isFinished());
        assertFalse(player.isDead());
        assertEquals(attackRegistry.size(), game.getAttackRegistry().size());
        checkAttackRegistryEntry(attackRegistry, 0);
        checkAttackRegistryEntry(attackRegistry, 1);
    }

    @Test
    void should_increase_player_level_after_fight() {
        //given
        mob.setHealth(1);
        for (int i = 0; i < 4; i++) {
            Mob curMob = new MagicianMob(new GameMap.Coordinate(i, i));
            game.addMob(curMob);
        }

        //when
        fightService.fight(game, mob, player);

        //then
        assertTrue(mob.isDead());
        assertFalse(game.isFinished());
        assertEquals(2, player.getLevel());
    }

    private void checkAttackRegistryEntry(ArrayList<Attack> attackRegistry, int i) {
        var expectedAttack = attackRegistry.get(i);
        var actualAttack = game.getAttackRegistry().get(i);
        assertEquals(expectedAttack.getDamage(), actualAttack.getDamage());
        assertEquals(expectedAttack.getHealth(), actualAttack.getHealth());
        assertEquals(expectedAttack.getTargetName(), actualAttack.getTargetName());
    }
}
