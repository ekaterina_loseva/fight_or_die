package logic;

import app.character.FigureType;
import app.logic.Game;
import app.logic.PlayerService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Scanner;

import static app.logic.PlayerService.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class PlayerServiceTest {

    private PlayerService playerService = new PlayerService();
    private Scanner scanner = Mockito.mock(Scanner.class);
    private Game game = new Game();

    @Test
    void should_create_new_warrior() {
        //given
        when(scanner.next()).thenReturn(String.valueOf(WARRIOR_CHOICE));

        //when
        playerService.createPlayer(scanner, game);

        //then
        assertEquals(FigureType.WARRIOR, game.getPlayer().getFigureType());
    }

    @Test
    void should_create_new_magician() {
        //given
        when(scanner.next()).thenReturn(String.valueOf(MAGICIAN_CHOICE));

        //when
        playerService.createPlayer(scanner, game);

        //then
        assertEquals(FigureType.MAGICIAN, game.getPlayer().getFigureType());
    }

    @Test
    void should_create_new_archer() {
        //given
        when(scanner.next()).thenReturn(String.valueOf(ARCHER_CHOICE));

        //when
        playerService.createPlayer(scanner, game);

        //then
        assertEquals(FigureType.ARCHER, game.getPlayer().getFigureType());
    }
}
