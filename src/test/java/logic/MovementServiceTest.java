package logic;

import app.character.hero.Magician;
import app.character.hero.Player;
import app.character.mob.MagicianMob;
import app.logic.FightService;
import app.logic.Game;
import app.logic.MovementService;
import app.ui.GameMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static app.common.AppConstants.FOOD_HEALTH;
import static app.logic.MovementService.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MovementServiceTest {

    @InjectMocks
    private MovementService movementService;

    @Mock
    private FightService fightService;

    private Game game = new Game();
    private Player player;

    @BeforeEach
    void setUp() {
        player = new Magician();
        player.setCoordinate(new GameMap.Coordinate(1, 1));
        game.setPlayer(player);
    }

    @Test
    void should_move_player_right_when_d_key_is_entered() {
        //when
        movementService.move(game, String.valueOf(KEY_RIGHT));

        //then
        assertEquals(2, player.getCoordinate().getX());
        assertEquals(1, player.getCoordinate().getY());
    }

    @Test
    void should_move_player_left_when_a_key_is_entered() {
        //when
        movementService.move(game, String.valueOf(KEY_LEFT));

        //then
        assertEquals(0, player.getCoordinate().getX());
        assertEquals(1, player.getCoordinate().getY());
    }

    @Test
    void should_move_player_up_when_w_key_is_entered() {
        //when
        movementService.move(game, String.valueOf(KEY_UP));

        //then
        assertEquals(1, player.getCoordinate().getX());
        assertEquals(0, player.getCoordinate().getY());
    }

    @Test
    void should_move_player_down_when_s_key_is_entered() {
        //when
        movementService.move(game, String.valueOf(KEY_DOWN));

        //then
        assertEquals(1, player.getCoordinate().getX());
        assertEquals(2, player.getCoordinate().getY());
    }

    @Test
    void should_do_nothing_on_wrong_input() {
        //when
        movementService.move(game, "test");

        //then
        assertEquals(1, player.getCoordinate().getX());
        assertEquals(1, player.getCoordinate().getY());
    }

    @Test
    void should_exit_game_when_0_key_is_entered() {
        //when
        movementService.move(game, String.valueOf(KEY_EXIT));

        //then
        assertEquals(1, player.getCoordinate().getX());
        assertEquals(1, player.getCoordinate().getY());
        assertTrue(game.isFinished());
        assertTrue(game.isForciblyStopped());
    }

    @Test
    void should_not_allow_player_to_attack_mob_beside_attack_range() {
        //given
        var mob = new MagicianMob(new GameMap.Coordinate(10, 10));
        game.addMob(mob);

        //when
        movementService.move(game, String.valueOf(KEY_FIGHT));

        //then
        verify(fightService, times(0)).fight(game, player, mob);
    }

    @Test
    void should_allow_player_to_attack_mob_in_attack_range() {
        //given
        var mob = new MagicianMob(new GameMap.Coordinate(1, 2));
        game.addMob(mob);

        //when
        movementService.move(game, String.valueOf(KEY_FIGHT));

        //then
        verify(fightService, times(1)).fight(game, player, mob);
    }

    @Test
    void should_allow_mob_to_attack_player_in_attack_range() {
        //given
        var mob = new MagicianMob(new GameMap.Coordinate(1, 3));
        game.addMob(mob);

        //when
        movementService.move(game, String.valueOf(KEY_DOWN));

        //then
        verify(fightService, times(1)).fight(game, mob, player);
    }

    @Test
    void should_heal_player_damage_when_f_key_is_entered() {
        //given
        int initialPlayerHealth = player.getHealth();
        game.addFoodCoordinate(new GameMap.Coordinate(1, 2));

        //when
        movementService.move(game, String.valueOf(KEY_DOWN));

        //then
        assertEquals(1, player.getCoordinate().getX());
        assertEquals(2, player.getCoordinate().getY());
        assertEquals(initialPlayerHealth + FOOD_HEALTH, player.getHealth());
        assertTrue(game.getFoodCoordinates().isEmpty());
    }
}
