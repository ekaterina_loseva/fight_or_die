package ui;

import app.character.hero.Warrior;
import app.character.mob.ArcherMob;
import app.character.mob.MagicianMob;
import app.character.mob.WarriorMob;
import app.logic.Game;
import app.ui.CellType;
import app.ui.GameMap;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static app.common.AppConstants.HERO_COORDINATE;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GameMapTest {

    @Test
    void should_generate_map_with_all_types_of_objects() {
        //give
        var gameMap = new GameMap();
        var game = new Game();

        var foodCoordinate = new GameMap.Coordinate(1, 1);
        game.addFoodCoordinate(foodCoordinate);

        var player = new Warrior();
        game.setPlayer(player);

        var magicianMobCoordinate = new GameMap.Coordinate(2, 2);
        var magicianMob = new MagicianMob(magicianMobCoordinate);
        game.addMob(magicianMob);

        var warriorMobCoordinate = new GameMap.Coordinate(3, 3);
        var warriorMob = new WarriorMob(warriorMobCoordinate);
        game.addMob(warriorMob);

        var archerMobCoordinate = new GameMap.Coordinate(4, 4);
        var archerMob = new ArcherMob(archerMobCoordinate);
        game.addMob(archerMob);

        //when
        Map<GameMap.Coordinate, CellType> result = gameMap.getMapWithGameObjects(game);

        //then
        assertEquals(CellType.HERO, result.get(HERO_COORDINATE));
        assertEquals(CellType.FOOD, result.get(foodCoordinate));
        assertEquals(CellType.MAGICIAN_MOB, result.get(magicianMobCoordinate));
        assertEquals(CellType.WARRIOR_MOB, result.get(warriorMobCoordinate));
        assertEquals(CellType.ARCHER_MOB, result.get(archerMobCoordinate));
    }
}
