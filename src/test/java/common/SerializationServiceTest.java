package common;

import app.character.hero.Magician;
import app.character.mob.WarriorMob;
import app.common.SerializationService;
import app.logic.Game;
import app.ui.GameMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class SerializationServiceTest {

    @Test
    void serialized_file_should_be_equal_to_deserialized() {
        //given
        var serializationService = new SerializationService();
        var player = new Magician();
        var mob = new WarriorMob(new GameMap.Coordinate(1, 1));
        var foodCoordinate = new GameMap.Coordinate(2, 2);
        var game = new Game();
        game.addMob(mob);
        game.addFoodCoordinate(foodCoordinate);
        game.setPlayer(player);
        game.forciblyStopGame();

        //when
        serializationService.serializeGame(game);
        var result = serializationService.deserializeGame();

        //then
        assertEquals(game.getFoodCoordinates(), result.getFoodCoordinates());

        var resultPlayer = result.getPlayer();
        assertEquals(player.getCoordinate(), resultPlayer.getCoordinate());
        assertEquals(player.getLevel(), resultPlayer.getLevel());
        assertEquals(player.getHealth(), resultPlayer.getHealth());
        assertEquals(player.getFigureType(), resultPlayer.getFigureType());

        var resultMob = result.getMobs().iterator().next();
        assertEquals(1, result.getMobs().size());
        assertEquals(mob.getFigureType(), resultMob.getFigureType());
        assertEquals(mob.getCoordinate(), resultMob.getCoordinate());
        assertEquals(mob.getHealth(), resultMob.getHealth());

        var resultFood = result.getFoodCoordinates().iterator().next();
        assertEquals(1, result.getFoodCoordinates().size());
        assertEquals(foodCoordinate, resultFood);

        assertFalse(result.isFinished());
        assertFalse(result.isForciblyStopped());
    }
}
