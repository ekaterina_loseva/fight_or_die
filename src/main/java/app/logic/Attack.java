package app.logic;

import app.character.Figure;

public class Attack {

    private Figure target;
    private int damage;
    private int health;

    Attack(Figure target, int damage) {
        this.target = target;
        this.damage = damage;
        this.health = target.getHealth();
    }

    public Attack(Figure target, int damage, int health) {
        this.target = target;
        this.damage = damage;
        this.health = health;
    }

    public String getTargetName() {
        return target.getName();
    }

    public int getDamage() {
        return damage;
    }

    public int getHealth() {
        return health;
    }
}
