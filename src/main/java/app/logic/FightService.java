package app.logic;

import app.character.Figure;
import app.common.TechnicalException;

import java.util.stream.Stream;

import static app.common.AppConstants.MOB_NUMBER;
import static java.util.function.Predicate.not;

public class FightService {

    public void fight(Game game, Figure firstFighter, Figure secondFighter) {
        do {
            attack(game, firstFighter, secondFighter);
            if (!firstFighter.isDead() && !secondFighter.isDead()) {
                attack(game, secondFighter, firstFighter);
            }
        } while (!firstFighter.isDead() && !secondFighter.isDead());


        var looser = Stream.of(firstFighter, secondFighter)
                .filter(Figure::isDead)
                .findFirst()
                .orElseThrow(() -> new TechnicalException("Looser can't be found"));

        var winner = Stream.of(firstFighter, secondFighter)
                .filter(not(Figure::isDead))
                .findFirst()
                .orElseThrow(() -> new TechnicalException("Winner can't be found"));

        if (looser.shouldCauseGameOverWhenDead()) {
            game.stopGame();
        } else {
            game.removeMob(looser);
            if (game.getMobs().size() % MOB_NUMBER == 0) {
                winner.increaseLevel();
            }
            if (game.getMobs().isEmpty()) {
                game.stopGame();
            }
        }
    }

    private void attack(Game game, Figure attacker, Figure target) {
        target.sustainDamage(attacker.getDamage());
        game.registerAttack(target, attacker.getDamage());
    }
}
