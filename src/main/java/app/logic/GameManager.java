package app.logic;

import app.common.SerializationService;
import app.ui.GameMap;

import java.util.Scanner;

import static app.common.LabelProvider.*;
import static app.ui.ConsoleTextProvider.*;

public class GameManager {

    private static final char NEW_GAME_CHOICE = '0';
    private static final char RESUME_GAME_CHOICE = '1';

    private Game game = new Game();
    private MovementService movementService = new MovementService();
    private PlayerService playerService = new PlayerService();
    private GameMap gameMap = new GameMap();
    private SerializationService serializationService = new SerializationService();

    public void startGame(Scanner input) {
        if (startNewGame(input)) {
            playerService.createPlayer(input, game);
            game.generateWorld();
        } else {
            game = serializationService.deserializeGame();
        }

        gameMap.generateMap(game);

        while (!game.isFinished()) {
            handleMovement(input.next());
        }

        if (game.isForciblyStopped()) {
            serializationService.serializeGame(game);
            printTextImmediate(SAVE_GAME_TEXT);
            return;
        }

        printTextImmediate(game.getPlayer().isDead() ? CONSOLATION_TEXT : CONGRATULATION_TEXT);
    }

    private void handleMovement(String input) {
        movementService.move(game, input);

        if (!game.isFinished()) {
            gameMap.generateMap(game);
        } else {
            addConsoleText(game.getAttackRegistry());
            printText();
        }
    }

    private boolean startNewGame(Scanner input) {
        printTextImmediate(WELCOME_TEXT);

        char choose;
        do {
            choose = input.next().charAt(0);
        } while (choose != NEW_GAME_CHOICE && choose != RESUME_GAME_CHOICE);

        return choose == NEW_GAME_CHOICE;
    }
}
