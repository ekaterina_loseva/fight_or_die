package app.logic;

import app.character.hero.Archer;
import app.character.hero.Magician;
import app.character.hero.Player;
import app.character.hero.Warrior;

import java.util.Scanner;

import static app.common.LabelProvider.CHOOSING_PLAYER_HELP_TEXT;
import static app.common.LabelProvider.CHOOSING_PLAYER_TEXT;
import static app.ui.ConsoleTextProvider.printTextImmediate;

public class PlayerService {

    public static final char WARRIOR_CHOICE = '1';
    public static final char MAGICIAN_CHOICE = '2';
    public static final char ARCHER_CHOICE = '3';
    public static final char HELP_TEXT = '0';

    public void createPlayer(Scanner input, Game game) {
        char choice;
        Player player;

        printTextImmediate(CHOOSING_PLAYER_TEXT);

        do {
            choice = input.next().charAt(0);

            if (choice == HELP_TEXT) {
                printTextImmediate(CHOOSING_PLAYER_HELP_TEXT);
            }

        } while (choice < WARRIOR_CHOICE || choice > ARCHER_CHOICE);

        switch (choice) {
            case WARRIOR_CHOICE:
                player = new Warrior();
                break;
            case MAGICIAN_CHOICE:
                player = new Magician();
                break;
            case ARCHER_CHOICE:
                player = new Archer();
                break;
            default:
                throw new UnsupportedOperationException("Unknown FigureType: " + choice);
        }
        game.setPlayer(player);
    }
}
