package app.logic;

import app.ui.GameMap;

import static app.common.AppConstants.*;

public class MovementService {

    public static final char KEY_UP = 'w';
    public static final char KEY_DOWN = 's';
    public static final char KEY_LEFT = 'a';
    public static final char KEY_RIGHT = 'd';
    public static final char KEY_EXIT = '0';
    public static final char KEY_FIGHT = 'f';

    private FightService fightService = new FightService();

    public void move(Game game, String input) {
        var playerCoordinate = game.getPlayer().getCoordinate();
        int playerCoordinateX = playerCoordinate.getX();
        int playerCoordinateY = playerCoordinate.getY();

        int newY, newX;
        char choice = input.toLowerCase().charAt(0);

        switch (choice) {
            case KEY_UP:
                newY = (playerCoordinateY - 1) < 0 ? playerCoordinateY - 1 + MAP_HIGHT : (playerCoordinateY - 1) % MAP_HIGHT;
                checkCoordinate(game, newY, playerCoordinateX);
                break;
            case KEY_LEFT:
                newX = (playerCoordinateX - 1) < 0 ? playerCoordinateX - 1 + MAP_WIDTH : (playerCoordinateX - 1) % MAP_WIDTH;
                checkCoordinate(game, playerCoordinateY, newX);
                break;
            case KEY_DOWN:
                newY = (playerCoordinateY + 1) % MAP_HIGHT;
                checkCoordinate(game, newY, playerCoordinateX);
                break;
            case KEY_RIGHT:
                newX = (playerCoordinateX + 1) % MAP_WIDTH;
                checkCoordinate(game, playerCoordinateY, newX);
                break;
            case KEY_FIGHT:
                startFight(game);
                break;
            case KEY_EXIT:
                game.forciblyStopGame();
                break;
        }
    }

    private void checkCoordinate(Game game, int newY, int newX) {
        var player = game.getPlayer();
        player.setCoordinate(new GameMap.Coordinate(newX, newY));

        game.getMobs()
            .stream()
            .filter(m -> m.canAttack(player))
            .findFirst()
            .ifPresent(m -> fightService.fight(game, m, player));

        var coordinate = new GameMap.Coordinate(newX, newY);
        if (game.getFoodCoordinates().contains(coordinate)) {
            player.healDamage(FOOD_HEALTH);
            game.removeFoodCoordinate(coordinate);
        }
    }

    private void startFight(Game game) {
        var player = game.getPlayer();
        game.getMobs()
            .stream()
            .filter(player::canAttack)
            .findFirst()
            .ifPresent(m -> fightService.fight(game, player, m));
    }
}
