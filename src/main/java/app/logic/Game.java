package app.logic;

import app.character.Figure;
import app.character.hero.Player;
import app.character.mob.ArcherMob;
import app.character.mob.MagicianMob;
import app.character.mob.Mob;
import app.character.mob.WarriorMob;
import app.common.AppConstants;
import app.ui.GameMap;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static app.common.AppConstants.*;

public class Game implements Serializable {

    private Player player;
    private boolean isFinished = false;
    private boolean isForciblyStopped = false;
    private Set<Figure> mobs = new HashSet<>();
    private Set<GameMap.Coordinate> foodCoordinates = new HashSet<>();
    private List<Attack> attackRegistry = new ArrayList<>();

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    void stopGame() {
        isFinished = true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isForciblyStopped() {
        return isForciblyStopped;
    }

    public void forciblyStopGame() {
        isFinished = true;
        isForciblyStopped = true;
    }

    public void initializeParametersForResumedGame() {
        isFinished = false;
        isForciblyStopped = false;
    }

    public Set<GameMap.Coordinate> getFoodCoordinates() {
        return Collections.unmodifiableSet(foodCoordinates);
    }

    void removeFoodCoordinate(GameMap.Coordinate coordinate) {
        foodCoordinates.remove(coordinate);
    }

    public void addFoodCoordinate(GameMap.Coordinate coordinate) {
        foodCoordinates.add(coordinate);
    }

    public Set<Figure> getMobs() {
        return Collections.unmodifiableSet(mobs);
    }

    public void addMob(Figure mob) {
        this.mobs.add(mob);
    }

    void removeMob(Figure mob) {
        mobs.remove(mob);
    }

    public List<Attack> getAttackRegistry() {
        return Collections.unmodifiableList(attackRegistry);
    }

    public void clearAttackRegistry() {
        attackRegistry.clear();
    }

    void registerAttack(Figure target, int damage) {
        attackRegistry.add(new Attack(target, damage));
    }

    public void generateWorld() {
        var random = new Random();
        createMobClass(random, WarriorMob::new);
        createMobClass(random, MagicianMob::new);
        createMobClass(random, ArcherMob::new);
        createFood(random);
    }

    private void createMobClass(Random random, Function<GameMap.Coordinate, Mob> function) {
        for (int i = 0; i < AppConstants.MOB_NUMBER; i++) {
            int width = random.nextInt(MAP_WIDTH - 1);
            int hight = random.nextInt(MAP_HIGHT - 1);
            var generatedCoordinate = new GameMap.Coordinate(width, hight);
            if (!generatedCoordinate.equals(HERO_COORDINATE)) {
                mobs.add(function.apply(new GameMap.Coordinate(width, hight)));
            }
        }
    }

    private void createFood(Random random) {
        Set<GameMap.Coordinate> mobCoordinates = mobs.stream().map(Figure::getCoordinate).collect(Collectors.toSet());
        for (int i = 0; i < FOOD_NUMBER; i++) {
            int width = random.nextInt(MAP_WIDTH);
            int hight = random.nextInt(MAP_HIGHT);
            var generatedCoordinate = new GameMap.Coordinate(width, hight);
            if (!generatedCoordinate.equals(HERO_COORDINATE) && !mobCoordinates.contains(generatedCoordinate)) {
                foodCoordinates.add(new GameMap.Coordinate(width, hight));
            }
        }
    }
}
