package app.common;

import app.logic.Game;

import java.io.*;

public class SerializationService {

    private static final String FILE_NAME = "game.ser";

    public void serializeGame(Game game) {
        try (var fileOut = new FileOutputStream(FILE_NAME)) {
            var out = new ObjectOutputStream(fileOut);
            out.writeObject(game);
            out.close();
        } catch (IOException ex) {
            throw new TechnicalException("Failed to serialize file: ", ex);
        }
    }

    public Game deserializeGame() {
        try (var fileIn = new FileInputStream(FILE_NAME)) {
            var in = new ObjectInputStream(fileIn);
            var game = (Game) in.readObject();
            in.close();
            game.initializeParametersForResumedGame();
            return game;
        } catch (IOException | ClassNotFoundException ex) {
            throw new TechnicalException("Failed to deserialize file: ", ex);
        }
    }
}
