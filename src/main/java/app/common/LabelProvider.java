package app.common;

public class LabelProvider {

    public static final String HERO_NAME = "Hero";
    public static final String WARRIOR_MOB_NAME = "Warrior-Mob";
    public static final String MAGICIAN_MOB_NAME = "Magician-Mob";
    public static final String ARCHER_MOB_NAME = "Archer-Mob";

    public static final String WELCOME_TEXT = "Welcome to the Fight Or Die game!\nDo you want to start a new game or resume an existing one? Please type 0 for a new game and 1 for existing one";

    public static final String CHOOSING_PLAYER_TEXT = "Please choose a character of the Hero:\n 1 - Warrior\n 2 - Magician\n 3 - Archer\n For more details type 0";
    public static final String CHOOSING_PLAYER_HELP_TEXT = "1 - Warrior: health = 10, attack range = 1 cell, damage = 1\n2 - Magician: health = 5, attack range = 2 cells, damage = 3\n3 - Archer: health = 7,attack range = 3 cells, damage = 2 \n";

    public static final String KEY_NAVIGATION_TEXT = "\nPlease use keys WASD for movement, F for fight and 0 for exit:";
    public static final String MOB_DESCRIPTION_TEXT = "Be sure:\n - W: red mob is a warrior -> health = 5, doesn't attack you first, damage = 1\n - M: purple mob is a magician -> health = 3, attack range = 1 cell, damage = 2\n - A: cyan mob is an archer -> health = 4, attack range = 2 cells, damage = 1\nUse food (F) for health restoring\nYour goal is to kill all mobs\n";

    public static final String HERO_INFORMATION_TEXT = "About you (H):\n - health = %d\n - attack range = %d cells\n - damage = %d\n - level = %d\n";

    public static final String FIGHT_TEXT = "%s got %d damage. New health = %d\n";

    public static final String SAVE_GAME_TEXT = "The game is saved! Good bye!";
    public static final String CONGRATULATION_TEXT = "Congratulations! You won!";
    public static final String CONSOLATION_TEXT = "You are dead but don't be upset, try again!";
}
