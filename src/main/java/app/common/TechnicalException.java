package app.common;

public class TechnicalException extends RuntimeException {

    TechnicalException(String problemDescription, Throwable cause) {
        super(problemDescription, cause);
    }

    public TechnicalException(String problemDescription) {
        super(problemDescription);
    }
}
