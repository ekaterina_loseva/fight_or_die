package app.common;

import app.ui.GameMap;

public class AppConstants {

    public static final int MAP_WIDTH = 15;
    public static final int MAP_HIGHT = 15;
    public static final int MAP_BORDER_WIDTH = MAP_WIDTH * 4 + 1;
    public static final int MOB_NUMBER = 4;
    public static final int FOOD_NUMBER = 10;
    public static final int FOOD_HEALTH = 3;
    public static final GameMap.Coordinate HERO_COORDINATE = new GameMap.Coordinate(0, 14);
}
