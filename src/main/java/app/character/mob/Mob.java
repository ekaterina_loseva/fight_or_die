package app.character.mob;

import app.character.FigureType;
import app.character.Figure;
import app.ui.GameMap;

public abstract class Mob extends Figure {

    Mob(String name, GameMap.Coordinate coordinate, FigureType figureType, int damage, int health, int attackRange) {
        super(name, figureType, damage, health, attackRange);
        setCoordinate(coordinate);
    }

    @Override
    public boolean shouldCauseGameOverWhenDead() {
        return false;
    }

    @Override
    public void increaseLevel() {
        // can not gain levels
    }
}
