package app.character.mob;

import app.character.FigureType;
import app.ui.GameMap;

import static app.common.LabelProvider.WARRIOR_MOB_NAME;

public class WarriorMob extends Mob {

    private static final int DAMAGE = 1;
    private static final int HEALTH = 5;
    private static final int ATTACK_RANGE = 1;

    public WarriorMob(GameMap.Coordinate coordinate) {
        super(WARRIOR_MOB_NAME, coordinate, FigureType.WARRIOR, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
