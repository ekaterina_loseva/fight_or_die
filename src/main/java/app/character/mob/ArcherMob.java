package app.character.mob;

import app.character.FigureType;
import app.ui.GameMap;

import static app.common.LabelProvider.ARCHER_MOB_NAME;

public class ArcherMob extends Mob {

    private static final int DAMAGE = 1;
    private static final int HEALTH = 4;
    private static final int ATTACK_RANGE = 3;

    public ArcherMob(GameMap.Coordinate coordinate) {
        super(ARCHER_MOB_NAME, coordinate, FigureType.ARCHER, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
