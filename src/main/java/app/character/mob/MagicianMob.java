package app.character.mob;

import app.character.FigureType;
import app.ui.GameMap;

import static app.common.LabelProvider.MAGICIAN_MOB_NAME;

public class MagicianMob extends Mob {

    private static final int DAMAGE = 2;
    private static final int HEALTH = 3;
    private static final int ATTACK_RANGE = 2;

    public MagicianMob(GameMap.Coordinate coordinate) {
        super(MAGICIAN_MOB_NAME, coordinate, FigureType.MAGICIAN, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
