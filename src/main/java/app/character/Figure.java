package app.character;

import app.ui.GameMap;

import java.io.Serializable;

public abstract class Figure implements Serializable {

    private String name;
    private FigureType figureType;
    private GameMap.Coordinate coordinate;
    private int damage;
    private int health;
    private int attackRange;
    private int level = 1;

    public Figure(String name, FigureType figureType, int damage, int health, int attackRange) {
        this.name = name;
        this.figureType = figureType;
        this.damage = damage;
        this.health = health;
        this.attackRange = attackRange;
    }

    public abstract void increaseLevel();

    public abstract boolean shouldCauseGameOverWhenDead();

    public String getName() {
        return name;
    }

    public FigureType getFigureType() {
        return figureType;
    }

    public GameMap.Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(GameMap.Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public int getDamage() {
        return damage;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void sustainDamage(int damage) {
        health = health - damage;
    }

    public void healDamage(int amountHealed) {
        health = health + amountHealed;
    }

    public boolean isDead(){
        return health < 1;
    }

    public int getLevel() {
        return level;
    }

    protected void setLevel(int level) {
        this.level = level;
    }

    public boolean canAttack(Figure target) {
        var targetCoordinate = target.coordinate;
        return coordinate.getX() == targetCoordinate.getX()
                && coordinate.getY() + attackRange > targetCoordinate.getY()
                && coordinate.getY() - attackRange < targetCoordinate.getY()
                || coordinate.getY() == targetCoordinate.getY()
                && coordinate.getX() + attackRange > targetCoordinate.getX()
                && coordinate.getX() - attackRange < targetCoordinate.getX();
    }
}
