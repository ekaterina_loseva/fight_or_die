package app.character.hero;

import app.character.Figure;
import app.character.FigureType;

import static app.common.AppConstants.HERO_COORDINATE;
import static app.common.LabelProvider.HERO_NAME;

public abstract class Player extends Figure {

    public Player(FigureType figureType, int damage, int health, int attackRange) {
        super(HERO_NAME, figureType, damage, health, attackRange);
        setCoordinate(HERO_COORDINATE);
    }

    @Override
    public int getDamage() {
        return super.getDamage() * getLevel();
    }

    @Override
    public boolean shouldCauseGameOverWhenDead() {
        return true;
    }

    @Override
    public void increaseLevel() {
        setLevel(getLevel() + 1);
    }
}
