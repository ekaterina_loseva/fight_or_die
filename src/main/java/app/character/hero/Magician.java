package app.character.hero;

import app.character.FigureType;

public class Magician extends Player {

    private static final int DAMAGE = 3;
    private static final int HEALTH = 5;
    private static final int ATTACK_RANGE = 3;

    public Magician() {
        super(FigureType.MAGICIAN, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
