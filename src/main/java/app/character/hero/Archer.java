package app.character.hero;

import app.character.FigureType;

public class Archer extends Player {

    private static final int DAMAGE = 2;
    private static final int HEALTH = 7;
    private static final int ATTACK_RANGE = 4;

    public Archer() {
        super(FigureType.ARCHER, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
