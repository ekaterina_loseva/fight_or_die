package app.character.hero;

import app.character.FigureType;

public class Warrior extends Player {

    private static final int DAMAGE = 1;
    private static final int HEALTH = 10;
    private static final int ATTACK_RANGE = 2;

    public Warrior() {
        super(FigureType.WARRIOR, DAMAGE, HEALTH, ATTACK_RANGE);
    }
}
