package app.character;

public enum FigureType {
    MAGICIAN,
    WARRIOR,
    ARCHER
}
