package app.ui;

enum ConsoleColors {

    RESET("\033[0m"),
    RED("\033[0;31m"),
    BLUE("\033[0;34m"),
    PURPLE("\033[0;35m"),
    CYAN("\033[0;36m"),
    WHITE("\033[0;38m"),
    NO_COLOR("");

    private String symbol;

    ConsoleColors(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
