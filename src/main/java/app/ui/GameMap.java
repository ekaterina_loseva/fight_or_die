package app.ui;

import app.character.FigureType;
import app.logic.Game;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static app.common.AppConstants.*;
import static app.common.LabelProvider.KEY_NAVIGATION_TEXT;
import static app.common.LabelProvider.MOB_DESCRIPTION_TEXT;
import static app.ui.ConsoleTextProvider.*;

public class GameMap {

    public void generateMap(Game game) {
        var map = getMapWithGameObjects(game);
        showMap(map, game);
    }

    public Map<Coordinate, CellType> getMapWithGameObjects(Game game) {
        var map = new HashMap<Coordinate, CellType>();

        for (int y = 0; y < MAP_HIGHT; y++) {
            for (int x = 0; x < MAP_WIDTH; x++) {
                map.put(new Coordinate(x, y), CellType.WAY);
            }
        }

        game.getMobs().forEach(mob -> map.put(mob.getCoordinate(), getMobType(mob.getFigureType())));
        game.getFoodCoordinates().forEach(coordinate -> map.put(coordinate, CellType.FOOD));
        map.put(game.getPlayer().getCoordinate(), CellType.HERO);

        return map;
    }

    private void showMap(Map<Coordinate, CellType> map, Game game) {
        addHeroInformationText(game.getPlayer());
        addConsoleText(MOB_DESCRIPTION_TEXT);

        for (int y = 0; y < MAP_HIGHT; y++) {
            for (int x = 0; x < MAP_BORDER_WIDTH; x++) {
                addRowDelimiter();
            }
            addEmptyLine();
            for (int x = 0; x < MAP_WIDTH; x++) {
                addColumnDelimiter();
                var cellTyp = map.get(new Coordinate(x, y));
                addColoredText(cellTyp.getColor(), cellTyp.getSymbol());
            }
            addColumnDelimiter();
            addEmptyLine();
        }
        for (int x = 0; x < MAP_BORDER_WIDTH; x++) {
            addRowDelimiter();
        }
        addConsoleText(game.getAttackRegistry());
        game.clearAttackRegistry();
        addConsoleText(KEY_NAVIGATION_TEXT);
        printText();
    }

    private CellType getMobType(FigureType figureType) {
        switch (figureType) {
            case WARRIOR:
                return CellType.WARRIOR_MOB;
            case ARCHER:
                return CellType.ARCHER_MOB;
            default:
                return CellType.MAGICIAN_MOB;
        }
    }

    public static class Coordinate implements Serializable {
        private int x;
        private int y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Coordinate that = (Coordinate) o;

            if (x != that.x) return false;
            return y == that.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }
}
