package app.ui;

import app.character.hero.Player;
import app.logic.Attack;

import java.util.List;

import static app.common.LabelProvider.FIGHT_TEXT;
import static app.common.LabelProvider.HERO_INFORMATION_TEXT;

public class ConsoleTextProvider {

    private static StringBuilder consoleText = new StringBuilder();

    static void addConsoleText(String text) {
        consoleText.append(text);
    }

    static void addRowDelimiter() {
        consoleText.append('-');
    }

    static void addColumnDelimiter() {
        consoleText.append('|');
    }

    static void addEmptyLine() {
        consoleText.append("\n");
    }

    static void addColoredText(String color, String text) {
        var coloredText = String.format("%s%s%s", color, text, ConsoleColors.RESET.getSymbol());
        consoleText.append(coloredText);
    }

    static void addHeroInformationText(Player player) {
        var coloredText = String.format(HERO_INFORMATION_TEXT, player.getHealth(), player.getAttackRange() - 1, player.getDamage(), player.getLevel());
        consoleText.append(coloredText);
    }

    public static void addConsoleText(List<Attack> attackRegistry) {
        addEmptyLine();
        attackRegistry.forEach(attack -> consoleText.append(String.format(FIGHT_TEXT, attack.getTargetName(), attack.getDamage(), attack.getHealth())));
    }

    public static void printText() {
        System.out.println(consoleText.toString());
        consoleText.setLength(0);
    }

    public static void printTextImmediate(String text) {
        System.out.println(text);
    }
}
