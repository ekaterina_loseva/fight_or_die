package app.ui;

public enum CellType {
    HERO(" H ", ConsoleColors.BLUE),
    WARRIOR_MOB(" W ", ConsoleColors.RED),
    MAGICIAN_MOB(" M ", ConsoleColors.PURPLE),
    ARCHER_MOB(" A ", ConsoleColors.CYAN),
    FOOD(" F ", ConsoleColors.WHITE),
    WAY("   ", ConsoleColors.NO_COLOR);

    private String symbol;
    private ConsoleColors color;

    CellType(String symbol, ConsoleColors color) {
        this.symbol = symbol;
        this.color = color;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getColor() {
        return color.getSymbol();
    }
}
