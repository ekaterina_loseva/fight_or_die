package app;

import app.logic.GameManager;

import java.util.Scanner;

public class FightOrDieApplication {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        var gameManager = new GameManager();
        gameManager.startGame(input);
    }
}
