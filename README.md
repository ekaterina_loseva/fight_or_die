# Fight or Die Game

Simple command line RPG game written in Java language

## General Info
The objective of the game is to kill all mobs on the map. For it the player creates the Hero which can explore the game world, gain experience through fighting mobs and eat food for health restoring. If the Hero has negative health he dies and loses the game.
The game functionality also supports saving and resuming the last game.

## Rules
World:
It's a map 15 x 15 cells which includes a Hero (Player), Food (F) and Mobs (W/M/A). 
The Hero can explore the world and fight mobs. Some mobs can initiate the fight if the Hero is in their attack range. 
The battle is automated and carried out in several stages until the first death. The attacking character starts, after his move is the opponent's turn. The Hero damage is calculated with the following formula: _Hero.Damage * Hero.Level_. Every mob has default damage for the class. The Hero's level increase occurs after killing every 4 mobs.
Food is used for health restoring: every piece restore 3 HP. 

Classes for player:

* Warrior: health = 10, attack range = 1 cell, damage = 1
* Magician: health = 5, attack range = 2 cells, damage = 3
* Archer: health = 7,attack range = 3 cells, damage = 2 

Classes for mob:

* Warrior (W): red mob is a warrior -> health = 5, doesn't attack you first, damage = 1
* Magician (M): purple mob is a magician -> health = 3, attack range = 1 cell, damage = 2
* Archer (A): cyan mob is an archer -> health = 4, attack range = 2 cells, damage = 1
 
## Setup
The main file is on `/src/main/java/app/FightOrDieApplication.java`

To start in command line:

* Navigate to folder `/src/main/java`
* Compile project with `javac -cp . app/FightOrDieApplication.java`
* Run with `java -Dfile.encoding=UTF8 -cp . app.FightOrDieApplication`
* Enjoy!

